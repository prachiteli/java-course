/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gipltraining.dowhile;

/**
 *
 * @author Prachi
 */
public class DoWhile {
    public static void main(String[] args) {
        int i=1;
        do { 
            int k=i*i;
            System.out.println("Square of "+i+" is "+k);
            i++;
        } while (i<=10);
    }
}
