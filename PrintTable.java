/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gipltraining.whileloop;

/**
 *
 * @author Prachi
 */
public class PrintTable {
    public static void main(String[] args){
        int i=1;
        
        while(i<=10){
            System.out.println(i+" Table:");
            int j=1;
            while(j<=10){
                int k=i*j;
                System.out.print(k+" ");
                j++;
                
            }
            System.out.println("");
            i++;
            
        }
    }
}
