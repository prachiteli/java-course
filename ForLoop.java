/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gipltraining.foorloop;

/**
 *
 * @author Prachi
 */
public class ForLoop {

    public static void main(String[] args) {
        for (int i = 1; i <= 10; i++) {
            System.out.println(i+" Table");
            for (int j = 1; j <= 10; j++) {
                int k=i*j;
                System.out.print(k+" ");
            }
            System.out.println(" ");
        }
    }
}
