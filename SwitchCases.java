/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gipltraining.switchcases;

/**
 *
 * @author Prachi
 */
public class SwitchCases {

    public static void main(String[] args) {
        int percentage = 75;
        String grade = "X";
        switch (percentage / 10) {
            case 1:
            case 2:
            case 3:
                grade = "fail";
                break;
            case 4:
                grade = "Pass";
                break;
            case 5:
            case 6:
                grade = "Third Class";
                break;
            case 7:
                grade = "second class";
                break;
            case 8:
            case 9:
            case 10:
                grade = "First class";
                break;
            default:
                grade = "Invalid";

        }
        System.out.println(percentage + ":" + grade);
    }
}
