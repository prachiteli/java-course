/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gipltraining.arithmeticoperationfunction;

/**
 *
 * @author Prachi
 */
public class ArithmeticOperationFunction {
    public static class Operation {
    public int add(int a, int b){
        return a+b;
    }
    public int sub(int a, int b){
        return a-b;
    }
    public int mul(int a, int b){
        return a*b;
    }
    public int div(int a, int b)throws IllegalAccessException{
        float division=a/b;
        int div=(int) division;
        return div;
    }
    
    public void print(int a){
        System.out.println(a);
    }
}
    
    public static void main(String[] args) throws IllegalAccessException{
        Operation operation=new Operation();
        operation.print(operation.add(4, 5));
        operation.print(operation.sub(5, 4));
        operation.print(operation.mul(4, 5));
        operation.print(operation.div(10, 3));
        
        
    }
}
