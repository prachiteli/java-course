/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gipltraining.foreach;

import java.util.ArrayList;

/**
 *
 * @author Prachi
 */
public class ForEach {
    public static void main(String[] args) {
        ArrayList<String> arllist=new ArrayList<>();
        arllist.add("Prachi");
        arllist.add("Suraj");
        arllist.add("Sadbudhi");
        arllist.add("Chetana");
        arllist.add("Saloni");
        arllist.add("Saurabh");
        for(String name:arllist){
            System.out.println(name);
        }
        
    }
}
