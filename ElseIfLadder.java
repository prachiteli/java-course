/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gipltraining.elseifladderdemo;

/**
 *
 * @author Prachi
 */
public class ElseIfLadder {
    public static void main(String[] args) {
        int a=25, b=3, c=4;
        if(a>b && a>c){
            System.out.println(a+" is greater than "+b+" & "+c);
        }
        else if(b>a && b>c){
            System.out.println(b+" is greater than"+a+" & "+c);
        }
        else{
            System.out.println(c+" is greater than "+a+" & "+b);
        }
    }
}
